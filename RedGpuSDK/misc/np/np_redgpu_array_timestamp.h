#pragma once

#define _np1_redCreateArrayTimestamp context
#define _np2_redCreateArrayTimestamp gpu
#define _np3_redCreateArrayTimestamp handleName
#define _np4_redCreateArrayTimestamp count
#define _np5_redCreateArrayTimestamp outArrayTimestamp
#define _np6_redCreateArrayTimestamp outStatuses
#define _np7_redCreateArrayTimestamp optionalFile
#define _np8_redCreateArrayTimestamp optionalLine
#define _np9_redCreateArrayTimestamp optionalUserData

#define _np1_redDestroyArrayTimestamp context
#define _np2_redDestroyArrayTimestamp gpu
#define _np3_redDestroyArrayTimestamp arrayTimestamp
#define _np4_redDestroyArrayTimestamp optionalFile
#define _np5_redDestroyArrayTimestamp optionalLine
#define _np6_redDestroyArrayTimestamp optionalUserData

#define _np1_redCallArrayTimestampWrite calls
#define _np2_redCallArrayTimestampWrite context
#define _np3_redCallArrayTimestampWrite arrayTimestamp
#define _np4_redCallArrayTimestampWrite index

#define _np1_redArrayTimestampRead context
#define _np2_redArrayTimestampRead gpu
#define _np3_redArrayTimestampRead arrayTimestamp
#define _np4_redArrayTimestampRead rangeFirst
#define _np5_redArrayTimestampRead rangeCount
#define _np6_redArrayTimestampRead out64BitTicksCounts
#define _np7_redArrayTimestampRead outStatuses
#define _np8_redArrayTimestampRead optionalFile
#define _np9_redArrayTimestampRead optionalLine
#define _np10_redArrayTimestampRead optionalUserData

