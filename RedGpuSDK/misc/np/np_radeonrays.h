#pragma once

#define _np1_rrCreateContext api_version
#define _np2_rrCreateContext api
#define _np3_rrCreateContext context

#define _np1_rrDestroyContext context

#define _np1_rrSetLogLevel log_level

#define _np1_rrSetLogFile filename

#define _np1_rrCmdBuildGeometry context
#define _np2_rrCmdBuildGeometry build_operation
#define _np3_rrCmdBuildGeometry build_input
#define _np4_rrCmdBuildGeometry build_options
#define _np5_rrCmdBuildGeometry temporary_buffer
#define _np6_rrCmdBuildGeometry geometry_buffer
#define _np7_rrCmdBuildGeometry command_stream

#define _np1_rrGetGeometryBuildMemoryRequirements context
#define _np2_rrGetGeometryBuildMemoryRequirements build_input
#define _np3_rrGetGeometryBuildMemoryRequirements build_options
#define _np4_rrGetGeometryBuildMemoryRequirements memory_requirements

#define _np1_rrCmdBuildScene context
#define _np2_rrCmdBuildScene build_input
#define _np3_rrCmdBuildScene build_options
#define _np4_rrCmdBuildScene temporary_buffer
#define _np5_rrCmdBuildScene scene_buffer
#define _np6_rrCmdBuildScene command_stream

#define _np1_rrGetSceneBuildMemoryRequirements context
#define _np2_rrGetSceneBuildMemoryRequirements build_input
#define _np3_rrGetSceneBuildMemoryRequirements build_options
#define _np4_rrGetSceneBuildMemoryRequirements memory_requirements

#define _np1_rrCmdIntersect context
#define _np2_rrCmdIntersect scene_buffer
#define _np3_rrCmdIntersect query
#define _np4_rrCmdIntersect rays
#define _np5_rrCmdIntersect ray_count
#define _np6_rrCmdIntersect indirect_ray_count
#define _np7_rrCmdIntersect query_output
#define _np8_rrCmdIntersect hits
#define _np9_rrCmdIntersect scratch
#define _np10_rrCmdIntersect command_stream

#define _np1_rrGetTraceMemoryRequirements context
#define _np2_rrGetTraceMemoryRequirements ray_count
#define _np3_rrGetTraceMemoryRequirements scratch_size

#define _np1_rrAllocateCommandStream context
#define _np2_rrAllocateCommandStream command_stream

#define _np1_rrReleaseCommandStream context
#define _np2_rrReleaseCommandStream command_stream

#define _np1_rrSumbitCommandStream context
#define _np2_rrSumbitCommandStream command_stream
#define _np3_rrSumbitCommandStream wait_event
#define _np4_rrSumbitCommandStream out_event

#define _np1_rrReleaseEvent context
#define _np2_rrReleaseEvent event

#define _np1_rrWaitEvent context
#define _np2_rrWaitEvent event

#define _np1_rrReleaseDevicePtr context
#define _np2_rrReleaseDevicePtr ptr

#define _np1_rrReleaseExternalCommandStream context
#define _np2_rrReleaseExternalCommandStream command_stream

