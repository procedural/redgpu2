#pragma once

#define _np1_redCreateSurfaceWin32 context
#define _np2_redCreateSurfaceWin32 gpu
#define _np3_redCreateSurfaceWin32 handleName
#define _np4_redCreateSurfaceWin32 win32Hinstance
#define _np5_redCreateSurfaceWin32 win32Hwnd
#define _np6_redCreateSurfaceWin32 outSurface
#define _np7_redCreateSurfaceWin32 outStatuses
#define _np8_redCreateSurfaceWin32 optionalFile
#define _np9_redCreateSurfaceWin32 optionalLine
#define _np10_redCreateSurfaceWin32 optionalUserData

#define _np1_redCreateSurfaceXlibOrXcb context
#define _np2_redCreateSurfaceXlibOrXcb gpu
#define _np3_redCreateSurfaceXlibOrXcb handleName
#define _np4_redCreateSurfaceXlibOrXcb xlibDisplay
#define _np5_redCreateSurfaceXlibOrXcb xlibWindow
#define _np6_redCreateSurfaceXlibOrXcb xcbConnection
#define _np7_redCreateSurfaceXlibOrXcb xcbWindow
#define _np8_redCreateSurfaceXlibOrXcb outSurface
#define _np9_redCreateSurfaceXlibOrXcb outStatuses
#define _np10_redCreateSurfaceXlibOrXcb optionalFile
#define _np11_redCreateSurfaceXlibOrXcb optionalLine
#define _np12_redCreateSurfaceXlibOrXcb optionalUserData

#define _np1_redCreatePresent context
#define _np2_redCreatePresent gpu
#define _np3_redCreatePresent queue
#define _np4_redCreatePresent handleName
#define _np5_redCreatePresent surface
#define _np6_redCreatePresent imagesCount
#define _np7_redCreatePresent imagesWidth
#define _np8_redCreatePresent imagesHeight
#define _np9_redCreatePresent imagesLayersCount
#define _np10_redCreatePresent imagesRestrictToAccess
#define _np11_redCreatePresent transform
#define _np12_redCreatePresent compositeAlpha
#define _np13_redCreatePresent vsyncMode
#define _np14_redCreatePresent clipped
#define _np15_redCreatePresent discardAfterPresent
#define _np16_redCreatePresent oldPresent
#define _np17_redCreatePresent outPresent
#define _np18_redCreatePresent outImages
#define _np19_redCreatePresent outTextures
#define _np20_redCreatePresent outStatuses
#define _np21_redCreatePresent optionalFile
#define _np22_redCreatePresent optionalLine
#define _np23_redCreatePresent optionalUserData

#define _np1_redDestroySurface context
#define _np2_redDestroySurface gpu
#define _np3_redDestroySurface surface
#define _np4_redDestroySurface optionalFile
#define _np5_redDestroySurface optionalLine
#define _np6_redDestroySurface optionalUserData

#define _np1_redDestroyPresent context
#define _np2_redDestroyPresent gpu
#define _np3_redDestroyPresent present
#define _np4_redDestroyPresent optionalFile
#define _np5_redDestroyPresent optionalLine
#define _np6_redDestroyPresent optionalUserData

#define _np1_redQueueFamilyIndexGetSupportsPresent context
#define _np2_redQueueFamilyIndexGetSupportsPresent gpu
#define _np3_redQueueFamilyIndexGetSupportsPresent queueFamilyIndex
#define _np4_redQueueFamilyIndexGetSupportsPresent supportsPresentOnWin32
#define _np5_redQueueFamilyIndexGetSupportsPresent supportsPresentOnXlib
#define _np6_redQueueFamilyIndexGetSupportsPresent supportsPresentOnXcb
#define _np7_redQueueFamilyIndexGetSupportsPresent supportsPresentOnSurface
#define _np8_redQueueFamilyIndexGetSupportsPresent outStatuses
#define _np9_redQueueFamilyIndexGetSupportsPresent optionalFile
#define _np10_redQueueFamilyIndexGetSupportsPresent optionalLine
#define _np11_redQueueFamilyIndexGetSupportsPresent optionalUserData

#define _np1_redSurfaceGetPresentFeatures context
#define _np2_redSurfaceGetPresentFeatures gpu
#define _np3_redSurfaceGetPresentFeatures surface
#define _np4_redSurfaceGetPresentFeatures outSurfacePresentFeatures
#define _np5_redSurfaceGetPresentFeatures outStatuses
#define _np6_redSurfaceGetPresentFeatures optionalFile
#define _np7_redSurfaceGetPresentFeatures optionalLine
#define _np8_redSurfaceGetPresentFeatures optionalUserData

#define _np1_redSurfaceGetCurrentPropertiesAndPresentLimits context
#define _np2_redSurfaceGetCurrentPropertiesAndPresentLimits gpu
#define _np3_redSurfaceGetCurrentPropertiesAndPresentLimits surface
#define _np4_redSurfaceGetCurrentPropertiesAndPresentLimits outSurfaceCurrentPropertiesAndPresentLimits
#define _np5_redSurfaceGetCurrentPropertiesAndPresentLimits outStatuses
#define _np6_redSurfaceGetCurrentPropertiesAndPresentLimits optionalFile
#define _np7_redSurfaceGetCurrentPropertiesAndPresentLimits optionalLine
#define _np8_redSurfaceGetCurrentPropertiesAndPresentLimits optionalUserData

#define _np1_redPresentGetImageIndex context
#define _np2_redPresentGetImageIndex gpu
#define _np3_redPresentGetImageIndex present
#define _np4_redPresentGetImageIndex signalCpuSignal
#define _np5_redPresentGetImageIndex signalGpuSignal
#define _np6_redPresentGetImageIndex outImageIndex
#define _np7_redPresentGetImageIndex outStatuses
#define _np8_redPresentGetImageIndex optionalFile
#define _np9_redPresentGetImageIndex optionalLine
#define _np10_redPresentGetImageIndex optionalUserData

#define _np1_redQueuePresent context
#define _np2_redQueuePresent gpu
#define _np3_redQueuePresent queue
#define _np4_redQueuePresent waitForAndUnsignalGpuSignalsCount
#define _np5_redQueuePresent waitForAndUnsignalGpuSignals
#define _np6_redQueuePresent presentsCount
#define _np7_redQueuePresent presents
#define _np8_redQueuePresent presentsImageIndex
#define _np9_redQueuePresent outPresentsStatus
#define _np10_redQueuePresent outStatuses
#define _np11_redQueuePresent optionalFile
#define _np12_redQueuePresent optionalLine
#define _np13_redQueuePresent optionalUserData

