#pragma once

#define _np1_redGpuVisTracingStart optionalFile
#define _np2_redGpuVisTracingStart optionalLine
#define _np3_redGpuVisTracingStart optionalUserData

#define _np1_redGpuVisTracingCapture optionalFile
#define _np2_redGpuVisTracingCapture optionalLine
#define _np3_redGpuVisTracingCapture optionalUserData

#define _np1_redGpuVisTracingStop optionalFile
#define _np2_redGpuVisTracingStop optionalLine
#define _np3_redGpuVisTracingStop optionalUserData

