#pragma once

#define _np1_redDebugArrayGetHandle context
#define _np2_redDebugArrayGetHandle gpu
#define _np3_redDebugArrayGetHandle outStatuses
#define _np4_redDebugArrayGetHandle optionalFile
#define _np5_redDebugArrayGetHandle optionalLine
#define _np6_redDebugArrayGetHandle optionalUserData

#define _np1_redDebugArrayCallPrint context
#define _np2_redDebugArrayCallPrint gpu
#define _np3_redDebugArrayCallPrint calls
#define _np4_redDebugArrayCallPrint outStatuses
#define _np5_redDebugArrayCallPrint optionalFile
#define _np6_redDebugArrayCallPrint optionalLine
#define _np7_redDebugArrayCallPrint optionalUserData

