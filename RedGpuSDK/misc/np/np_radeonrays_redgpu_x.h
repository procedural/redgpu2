#pragma once

#define _np1_rrGetDevicePtrFromD3D12Resource context
#define _np2_rrGetDevicePtrFromD3D12Resource resource
#define _np3_rrGetDevicePtrFromD3D12Resource offset
#define _np4_rrGetDevicePtrFromD3D12Resource device_ptr

#define _np1_rrCreateContextDX api_version
#define _np2_rrCreateContextDX d3d_device
#define _np3_rrCreateContextDX command_queue
#define _np4_rrCreateContextDX context

#define _np1_rrGetCommandStreamFromD3D12CommandList context
#define _np2_rrGetCommandStreamFromD3D12CommandList command_list
#define _np3_rrGetCommandStreamFromD3D12CommandList command_stream

